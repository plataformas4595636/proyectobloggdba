from bson import ObjectId
from bda_connection import get_database

class Article:
    def __init__(self):
        db = get_database()
        self.collection = db["articles"]
    
    def add_article(self, title, date, text):
        new_article = {
            'title': title,
            'date': date,
            'text': text
        }
        self.collection.insert_one(new_article)
    
    def eliminar_articulo(self, article_id):
        res = self.collection.delete_one({'_id': ObjectId(article_id)})
        return res.deleted_count > 0
    
    def modificar_articulo(self, article_id, new_title, new_date, new_text):
        res = self.collection.update_one(
            {'_id': ObjectId(article_id)},
            {'$set': {'title': new_title, 'date': new_date, 'text': new_text}}
        )
        return res.modified_count > 0
    
    def obtener_todos_los_articulos(self):
        return list(self.collection.find())

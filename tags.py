from bda_connection import get_database
from bson import ObjectId

class Tag:
    def __init__(self):
        db = get_database()
        self.collection = db["tags"]
    
    def agregar_etiqueta(self, nombre, url, article_id):
        new_tag = {
            'name': nombre,
            'url': url,
            'article_id': article_id
        }
        self.collection.insert_one(new_tag)

    def eliminar_etiqueta(self, tag_id):
        res = self.collection.delete_one({'_id': ObjectId(tag_id)})
        return res.deleted_count > 0

    def modificar_etiqueta(self, tag_id, new_name, new_url):
        res = self.collection.update_one(
            {'_id': ObjectId(tag_id)},
            {'$set': {'name': new_name, 'url': new_url}}
        )
        return res.modified_count > 0

    def obtener_todas_las_etiquetas(self):
        return list(self.collection.find())

    def buscar_etiqueta(self, tag_id):
        return self.collection.find_one({'_id': ObjectId(tag_id)})

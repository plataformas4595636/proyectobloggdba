from bda_connection import get_database
from bson import ObjectId

class Category:
    def __init__(self):
        db = get_database()
        self.collection = db["categories"]
    
    def agregar_categoria(self, nombre, url, article_id):
        new_category = {
            'name': nombre,
            'url': url,
            'article_id': article_id
        }
        self.collection.insert_one(new_category)

    def eliminar_categoria(self, category_id):
        res = self.collection.delete_one({'_id': ObjectId(category_id)})
        return res.deleted_count > 0

    def modificar_categoria(self, category_id, new_name, new_url):
        res = self.collection.update_one(
            {'_id': ObjectId(category_id)},
            {'$set': {'name': new_name, 'url': new_url}}
        )
        return res.modified_count > 0

    def obtener_todas_las_categorias(self):
        return list(self.collection.find())

    def buscar_categoria(self, category_id):
        return self.collection.find_one({'_id': ObjectId(category_id)})

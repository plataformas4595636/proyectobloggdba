# Sistema de Gestión de Blog

Este es un sistema de gestión de blog desarrollado en Python que utiliza una base de datos MongoDB para almacenar usuarios, artículos, comentarios, etiquetas y categorías.

## Funcionalidades

- **Gestión de Usuarios:** Permite agregar, eliminar, modificar y ver todos los usuarios registrados en el blog.
- **Gestión de Artículos:** Permite agregar, eliminar, modificar y ver todos los artículos publicados en el blog. También permite asociar etiquetas y categorías a los artículos.
- **Gestión de Comentarios:** Permite agregar, eliminar, modificar y ver todos los comentarios realizados en los artículos del blog.
- **Gestión de Etiquetas:** Permite agregar, eliminar, modificar y ver todas las etiquetas asociadas a los artículos del blog.
- **Gestión de Categorías:** Permite agregar, eliminar, modificar y ver todas las categorías asociadas a los artículos del blog.

## Requisitos

- Python 3.x instalado en tu sistema.
- Una instancia de MongoDB ejecutándose localmente o en un servidor remoto.

## Instalación

1. Clona o descarga este repositorio en tu máquina local.
2. Asegúrate de tener Python 3.x instalado asi como MongoDB.
3. Instala las dependencias necesarias ejecutando `pip install -r requirements.txt`.
4. Configura la conexión a tu base de datos MongoDB en el archivo `bda_connection.py`.
5. Ejecuta el programa ejecutando `python menu.py` desde tu terminal.

## Uso

1. Ejecuta el programa siguiendo las instrucciones de instalación.
2. Selecciona una opción del menú principal para gestionar usuarios, artículos, comentarios, etiquetas o categorías.
3. Sigue las instrucciones en pantalla para realizar las operaciones deseadas.



## Autor

Brayan Ricardo Carrete Martínez - 307746
## Licencia

Sin licencia



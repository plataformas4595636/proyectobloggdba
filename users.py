from bson import ObjectId
from bda_connection import get_database

class Users:
    def __init__(self):
        db = get_database()
        self.collection = db["users"]
    
    def agregar_usuario(self, name, email):
        new_user = {
            'name': name,
            'email': email
        }
        self.collection.insert_one(new_user)
    
    def eliminar_usuario(self, user_id):
        res = self.collection.delete_one({'_id': ObjectId(user_id)})
        return res.deleted_count > 0
    
    def modificar_usuario(self, user_id, new_name, new_email):
        res = self.collection.update_one(
            {'_id': ObjectId(user_id)},
            {'$set': {'name': new_name, 'email': new_email}}
        )
        return res.modified_count > 0
    
    def obtener_todos_los_usuarios(self):
        return list(self.collection.find())


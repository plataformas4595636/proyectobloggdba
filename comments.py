from bda_connection import get_database
from bson import ObjectId

class Comment:

    def __init__(self):
        db = get_database()
        self.collection = db["comments"]
    
    def agregar_comentario(self, nombre, url, user_id, article_id):
        new_comment = {
            'name': nombre,
            'url': url,
            'user_id': ObjectId(user_id),
            'article_id': ObjectId(article_id)
        }
        self.collection.insert_one(new_comment)

    def eliminar_comentario(self, comment_id):
        res = self.collection.delete_one({'_id': ObjectId(comment_id)})
        return res.deleted_count > 0

    def modificar_comentario(self, comment_id, new_name, new_url):
        res = self.collection.update_one(
            {'_id': ObjectId(comment_id)},
            {'$set': {'name': new_name, 'url': new_url}}
        )
        return res.modified_count > 0

    def buscar_comentario(self, comment_id):
        return self.collection.find_one({'_id': ObjectId(comment_id)})

    def obtener_comentarios(self):
        return list(self.collection.find())

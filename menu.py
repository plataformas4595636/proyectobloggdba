from bda_connection import get_database
from users import Users
from articles import Article
from comments import Comment
from tags import Tag
from categories import Category
from bson import ObjectId

def main():
    while True:
        print("\n----- Menú Principal -----")
        print("1. Gestionar Usuarios")
        print("2. Gestionar Artículos")
        print("3. Gestionar Comentarios")
        print("4. Salir")

        op_menup = input("Seleccione una opción: ")

        if op_menup == '1':
            users = Users()  # Crear una instancia de Users
            while True:
                print("\n----- Menú Usuarios -----")
                print("1. Agregar Usuario")
                print("2. Eliminar Usuario")
                print("3. Modificar Usuario")
                print("4. Ver Todos los Usuarios")
                print("5. Volver al Menú Principal")

                op_menus = input("Seleccione una opción: ")

                if op_menus == '1':
                    name = input("Ingrese el nombre del usuario: ")
                    email = input("Ingrese el correo electrónico del usuario: ")
                    users.agregar_usuario(name, email)  # Llamar al método desde la instancia
                    print("Usuario agregado exitosamente.")
                
                elif op_menus == '2':
                    user_id = input("Ingrese el ID del usuario a eliminar: ")
                    if users.eliminar_usuario(user_id):
                        print("Usuario eliminado exitosamente.")
                    else:
                        print("No se pudo eliminar el usuario.")

                elif op_menus == '3':
                    user_id = input("Ingrese el ID del usuario a modificar: ")
                    new_name = input("Ingrese el nuevo nombre del usuario: ")
                    new_email = input("Ingrese el nuevo correo electrónico del usuario: ")
                    if users.modificar_usuario(user_id, new_name, new_email):
                        print("Usuario modificado exitosamente.")
                    else:
                        print("No se pudo modificar el usuario.")
                
                elif op_menus == '4':
                    usuarios = users.obtener_todos_los_usuarios()
                    for usuario in usuarios:
                        user_id = usuario.get('_id')
                        name = usuario.get('name', 'N/A')
                        email = usuario.get('email', 'N/A')
                        print(f"ID: {user_id}, Nombre: {name}, Email: {email}")
                
                elif op_menus == '5':
                    break
                
                else:
                    print("Opción no válida. Intente de nuevo.")

        elif op_menup == '2':
            article = Article()  # Crear una instancia de Article
            while True:
                print("\n----- Menú Artículos -----")
                print("1. Agregar Artículo")
                print("2. Eliminar Artículo")
                print("3. Modificar Artículo")
                print("4. Ver Todos los Artículos")
                print("5. Gestionar Etiquetas")
                print("6. Gestionar Categorías")
                print("7. Volver al Menú Principal")
                op_menuart = input("Seleccione una opción: ")

                if op_menuart == '1':
                    title = input("Ingrese el título del artículo: ")
                    date = input("Ingrese la fecha del artículo: ")
                    text = input("Ingrese el texto del artículo: ")
                    article.add_article(title, date, text)  # Llamar al método desde la instancia
                    print("Artículo agregado exitosamente.")

                elif op_menuart == '2':
                    article_id = input("Ingrese el ID del artículo a eliminar: ")
                    if article.eliminar_articulo(article_id):
                        print("Artículo eliminado exitosamente.")
                    else:
                        print("ID de artículo no válido o no se pudo eliminar el artículo.")
                
                elif op_menuart == '3':
                    article_id = input("Ingrese el ID del artículo a modificar: ")
                    new_title = input("Ingrese el nuevo título del artículo: ")
                    new_date = input("Ingrese la nueva fecha del artículo: ")
                    new_text = input("Ingrese el nuevo texto del artículo: ")
                    if article.modificar_articulo(article_id, new_title, new_date, new_text):
                        print("Artículo modificado exitosamente.")
                    else:
                        print("ID de artículo no válido o no se pudo modificar el artículo.")
                
                elif op_menuart == '4':
                    articulos = article.obtener_todos_los_articulos()
                    for articulo in articulos:
                        article_id = articulo.get('_id')
                        title = articulo.get('title', 'N/A')
                        date = articulo.get('date', 'N/A')
                        text = articulo.get('text', 'N/A')
                        print(f"ID: {article_id}, Título: {title}, Fecha: {date}, Texto: {text}")
                
                elif op_menuart == '5':
                    tag = Tag()  # Crear una instancia de Tag
                    while True:
                        print("\n----- Menú Etiquetas -----")
                        print("1. Agregar Etiqueta")
                        print("2. Eliminar Etiqueta")
                        print("3. Modificar Etiqueta")
                        print("4. Ver Todas las Etiquetas")
                        print("5. Volver al Menú Artículos")

                        op_menutag = input("Seleccione una opción: ")

                        if op_menutag == '1':
                            nombre = input("Ingrese el nombre de la etiqueta: ")
                            url = input("Ingrese la URL de la etiqueta: ")
                            article_id = input("Ingrese el ID del artículo asociado: ")
                            tag.agregar_etiqueta(nombre, url, article_id)
                            print("Etiqueta agregada correctamente.")

                        elif op_menutag == '2':
                            tag_id = input("Ingrese el ID de la etiqueta a eliminar: ")
                            if tag.eliminar_etiqueta(tag_id):
                                print("Etiqueta eliminada correctamente.")
                            else:
                                print("No se pudo eliminar la etiqueta.")

                        elif op_menutag == '3':
                            tag_id = input("Ingrese el ID de la etiqueta a modificar: ")
                            new_name = input("Ingrese el nuevo nombre de la etiqueta: ")
                            new_url = input("Ingrese la nueva URL de la etiqueta: ")
                            if tag.modificar_etiqueta(tag_id, new_name, new_url):
                                print("Etiqueta modificada correctamente.")
                            else:
                                print("No se pudo modificar la etiqueta.")

                        elif op_menutag == '4':
                            etiquetas = tag.obtener_todas_las_etiquetas()
                            if etiquetas:
                                print("Etiquetas:")
                                for etiqueta in etiquetas:
                                    print(f"ID: {etiqueta['_id']}, Nombre: {etiqueta['name']}, URL: {etiqueta['url']}")
                            else:
                                print("No hay etiquetas disponibles.")


                        elif op_menutag == '5':
                            break

                        else:
                            print("Opción no válida. Intente de nuevo.")

                elif op_menuart == '6':
                    # Gestionar categorías
                    category = Category()  # Crear una instancia de Category
                    while True:
                        print("\n----- Menú Categorías -----")
                        print("1. Agregar Categoría")
                        print("2. Eliminar Categoría")
                        print("3. Modificar Categoría")
                        print("4. Ver Todas las Categorías")
                        print("5. Volver al Menú Artículos")

                        op_menucat = input("Seleccione una opción: ")

                        if op_menucat == '1':
                            nombre = input("Ingrese el nombre de la categoría: ")
                            url = input("Ingrese la URL de la categoría: ")
                            article_id = input("Ingrese el ID del artículo asociado: ")
                            category.agregar_categoria(nombre, url, article_id)
                            print("Categoría agregada correctamente.")

                        elif op_menucat == '2':
                            category_id = input("Ingrese el ID de la categoría a eliminar: ")
                            if category.eliminar_categoria(category_id):
                                print("Categoría eliminada correctamente.")
                            else:
                                print("No se pudo eliminar la categoría.")

                        elif op_menucat == '3':
                            category_id = input("Ingrese el ID de la categoría a modificar: ")
                            new_name = input("Ingrese el nuevo nombre de la categoría: ")
                            new_url = input("Ingrese la nueva URL de la categoría: ")
                            if category.modificar_categoria(category_id, new_name, new_url):
                                print("Categoría modificada correctamente.")
                            else:
                                print("No se pudo modificar la categoría.")

                        elif op_menucat == '4':
                            categorias = category.obtener_todas_las_categorias()
                            if categorias:
                                print("Categorías:")
                                for cat in categorias:
                                    print(f"ID: {cat['_id']}, Nombre: {cat['name']}, URL: {cat['url']}")
                            else:
                                print("No hay categorías disponibles.")


                        elif op_menucat == '5':
                            break

                        else:
                            print("Opción no válida. Intente de nuevo.")

                elif op_menuart == '7':
                    break

                else:
                    print("Opción no válida. Intente de nuevo.")

        elif op_menup == '3':
            comment = Comment()  # Crear una instancia de Comment
            while True:
                print("\n----- Menú Comentarios -----")
                print("1. Agregar Comentario")
                print("2. Eliminar Comentario")
                print("3. Modificar Comentario")
                print("4. Ver Todos los Comentarios")
                print("5. Volver al Menú Principal")

                op_menucom = input("Seleccione una opción: ")

                if op_menucom == '1':
                    nombre = input("Ingrese el nombre del comentario: ")
                    url = input("Ingrese el URL del comentario: ")
                    user_id = input("Ingrese el ID del usuario: ")
                    article_id = input("Ingrese el ID del artículo: ")
                    try:
                        ObjectId(user_id)
                        ObjectId(article_id)
                        comment.agregar_comentario(nombre, url, user_id, article_id)
                        print("Comentario agregado exitosamente.")
                    except:
                        print("ID de usuario o artículo no válido.")

                elif op_menucom == '2':
                    comment_id = input("Ingrese el ID del comentario a eliminar: ")
                    if comment.eliminar_comentario(comment_id):
                        print("Comentario eliminado exitosamente.")
                    else:
                        print("ID de comentario no válido o no se pudo eliminar el comentario.")
                
                elif op_menucom == '3':
                    comment_id = input("Ingrese el ID del comentario a modificar: ")
                    new_name = input("Ingrese el nuevo nombre del comentario: ")
                    new_url = input("Ingrese el nuevo URL del comentario: ")
                    if comment.modificar_comentario(comment_id, new_name, new_url):
                        print("Comentario modificado exitosamente.")
                    else:
                        print("ID de comentario no válido o no se pudo modificar el comentario.")
                
                elif op_menucom == '4':
                    comentarios = comment.obtener_comentarios()
                    for comentario in comentarios:
                        comment_id = comentario.get('_id')
                        name = comentario.get('name', 'N/A')
                        url = comentario.get('url', 'N/A')
                        user_id = comentario.get('user_id', 'N/A')
                        article_id = comentario.get('article_id', 'N/A')
                        print(f"ID: {comment_id}, Nombre: {name}, URL: {url}, ID de Usuario: {user_id}, ID de Artículo: {article_id}")
                
                elif op_menucom == '5':
                    break
                
                else:
                    print("Opción no válida. Intente de nuevo.")

        elif op_menup == '4':
            print("SALIENDO")
            break

        else:
            print("Opción no válida. Intente de nuevo.")

if __name__ == "__main__":
    main()


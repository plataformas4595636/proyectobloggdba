from pymongo import MongoClient

# funcion para conectar a la base de datos
# retorna la conexion a la base de datos 'blog'
def get_database():
    CONNECTION_STRING = "mongodb://localhost:27017/blog"
    client = MongoClient(CONNECTION_STRING)
    return client['blog']

# se manda a llamar la funcion para conectar a la base de datos
get_database()

if __name__ == "__main__":
    conn = get_database()
    print("Connection stablished")